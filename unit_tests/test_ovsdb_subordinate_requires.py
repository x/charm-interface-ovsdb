# Copyright 2020 Canonical Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import mock

from ovsdb_subordinate import requires

import charms_openstack.test_utils as test_utils


_hook_args = {}


class TestOVSDBSubordinateRequires(test_utils.PatchHelper):

    def setUp(self):
        super().setUp()
        self.target = requires.OVSDBSubordinateRequires('some-relation', [])
        self._patches = {}
        self._patches_start = {}

    def tearDown(self):
        self.target = None
        for k, v in self._patches.items():
            v.stop()
            setattr(self, k, None)
        self._patches = None
        self._patches_start = None

    def patch_target(self, attr, return_value=None):
        mocked = mock.patch.object(self.target, attr)
        self._patches[attr] = mocked
        started = mocked.start()
        started.return_value = return_value
        self._patches_start[attr] = started
        setattr(self, attr, started)

    def patch_topublish(self):
        self.patch_target('_relations')
        relation = mock.MagicMock()
        to_publish = mock.PropertyMock()
        type(relation).to_publish = to_publish
        self._relations.__iter__.return_value = [relation]
        return relation.to_publish

    def test_chassis_name(self):
        self.patch_target('_all_joined_units')
        self._all_joined_units.received.get.return_value = 'fakename'
        self.assertEquals(self.target.chassis_name, 'fakename')
        self._all_joined_units.received.get.assert_called_once_with(
            'chassis-name', '')

    def test_ovn_configured(self):
        self.patch_target('_all_joined_units')
        self._all_joined_units.received.get.return_value = True
        self.assertEquals(self.target.ovn_configured, True)
        self._all_joined_units.received.get.assert_called_once_with(
            'ovn-configured', False)

    def test__add_interface_request(self):
        to_publish = self.patch_topublish()
        to_publish.get.return_value = {}
        self.target._add_interface_request('br-ex', 'eth0', {'data': ''})
        to_publish.__setitem__.assert_called_once_with(
            'create-interfaces', {'br-ex': {'eth0': {'data': ''}}})

    def test__interfrace_requests(self):
        self.patch_target('_relations')
        relation = mock.MagicMock()
        self._relations.__iter__.return_value = [relation]
        relation.to_publish_raw.get.return_value = 'aValue'
        self.assertEquals(self.target._interface_requests(), 'aValue')
        relation.to_publish_raw.get.assert_called_once_with(
            'create-interfaces')

    def test_create_interface(self):
        self.patch_target('_add_interface_request')
        self.patch_object(requires.reactive, 'clear_flag')
        ifdata = {
            'type': 'internal',
            'external-ids': {
                'iface-id': 'fakeuuid',
                'iface-status': 'active',
                'attached-mac': 'fakemac',
            },
        }
        self.target.create_interface('br-ex', 'eth0', 'fakemac', 'fakeuuid')
        self._add_interface_request.assert_called_once_with(
            'br-ex', 'eth0', ifdata)
        self.clear_flag.assert_called_once_with(
            'some-relation.interfaces.created')
        self._add_interface_request.reset_mock()
        ifdata['type'] = 'someothervalue'
        ifdata['external-ids']['iface-status'] = 'inactive'
        self.target.create_interface('br-ex', 'eth0', 'fakemac', 'fakeuuid',
                                     iftype='someothervalue',
                                     ifstatus='inactive')
        self._add_interface_request.assert_called_once_with(
            'br-ex', 'eth0', ifdata)

    def test_joined(self):
        self.patch_object(requires.reactive, 'set_flag')
        self.target.joined()
        self.set_flag.assert_called_once_with('some-relation.connected')
        # available flag should be set when there is a value for chassis_name
        self.patch_target('_all_joined_units')
        self._all_joined_units.received.get.return_value = 'some.fqdn'
        self.set_flag.reset_mock()
        self.target.joined()
        self.set_flag.assert_has_calls([
            mock.call('some-relation.connected'),
            mock.call('some-relation.available'),
        ])

    def test_broken(self):
        self.patch_object(requires.reactive, 'clear_flag')
        self.target.broken()
        self.clear_flag.assert_has_calls([
            mock.call('some-relation.available'),
            mock.call('some-relation.connected'),
        ])

    def test_new_requests(self):
        self.patch_target('_interface_requests')
        self.patch_target('_all_joined_units')
        self.patch_object(requires, 'hash_hexdigest')
        self.hash_hexdigest.return_value = 'fakehash'
        self._interface_requests.return_value = 'fakerequests'
        self.patch_object(requires.reactive, 'set_flag')
        self.patch_object(requires.reactive, 'clear_flag')
        self.target.new_requests()
        self.hash_hexdigest.assert_called_once_with('fakerequests')
        self.assertFalse(self.set_flag.called)
        self.assertFalse(self.clear_flag.called)
        self._all_joined_units.received.__getitem__.return_value = 'fakehash'
        self.target.new_requests()
        self.set_flag.assert_called_once_with(
            'some-relation.interfaces.created')
        self.clear_flag.assert_called_once_with(
            'endpoint.some-relation.changed.interfaces-created')

    def test_chassis_certificates(self):
        self.patch_target('_all_joined_units')
        self._all_joined_units.received.get.return_value = {'fake': 'cert'}
        self.assertEquals(self.target.chassis_certificates, {'fake': 'cert'})
        self._all_joined_units.received.get.assert_called_once_with(
            'chassis-certificates', {})
