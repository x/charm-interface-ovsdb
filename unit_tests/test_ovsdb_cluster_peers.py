# Copyright 2019 Canonical Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import mock

from ovsdb_cluster import peers

import charms_openstack.test_utils as test_utils


_hook_args = {}


class TestOVSDBClusterPeer(test_utils.PatchHelper):

    def setUp(self):
        super().setUp()
        self.target = peers.OVSDBClusterPeer('some-relation', [])
        self._patches = {}
        self._patches_start = {}

    def tearDown(self):
        self.target = None
        for k, v in self._patches.items():
            v.stop()
            setattr(self, k, None)
        self._patches = None
        self._patches_start = None

    def patch_target(self, attr, return_value=None):
        mocked = mock.patch.object(self.target, attr)
        self._patches[attr] = mocked
        started = mocked.start()
        started.return_value = return_value
        self._patches_start[attr] = started
        setattr(self, attr, started)

    def test_expected_peers_available(self):
        # self.patch_target('_all_joined_units')
        self.patch_object(peers.ch_core.hookenv, 'expected_peer_units')
        self.patch_target('_relations')
        self.target._all_joined_units = ['aFakeUnit']
        self.expected_peer_units.return_value = ['aFakeUnit']
        relation = mock.MagicMock()
        unit = mock.MagicMock()
        relation.units.__iter__.return_value = [unit]
        self._relations.__iter__.return_value = [relation]
        unit.received.get.return_value = '127.0.0.1'
        self.assertTrue(self.target.expected_peers_available())
        unit.received.get.assert_called_once_with('bound-address')
        unit.received.get.return_value = ''
        self.assertFalse(self.target.expected_peers_available())
        self.expected_peer_units.return_value = ['firstFakeUnit',
                                                 'secondFakeUnit']
        unit.received.get.return_value = '127.0.0.1'
        self.assertFalse(self.target.expected_peers_available())

    def test_db_nb_connection_strs(self):
        relation = mock.MagicMock()
        relation.relation_id = 'some-endpoint:42'
        self.patch_target('expand_name')
        self.expand_name.return_value = 'some-relation'
        self.patch_object(peers.ch_core.hookenv, 'network_get')
        self.network_get.return_value = {
            'bind-addresses': [
                {
                    'macaddress': '',
                    'interfacename': '',
                    'addresses': [
                        {
                            'hostname': '',
                            'address': '42.42.42.42',
                            'cidr': ''
                        },
                    ],
                },
            ],
            'egress-subnets': ['42.42.42.42/32'],
            'ingress-addresses': ['42.42.42.42'],
        }
        unit = mock.MagicMock()
        unit.received = {'bound-address': '1.2.3.4'}
        unit2 = mock.MagicMock()
        unit2.received = {'bound-address': '2.3.4.5'}
        relation.units.__iter__.return_value = [unit, unit2]
        self.patch_target('_relations')
        self._relations.__iter__.return_value = [relation]
        self.assertEquals(
            list(self.target.db_nb_connection_strs),
            ['ssl:42.42.42.42:6641',
             'ssl:1.2.3.4:6641',
             'ssl:2.3.4.5:6641'])

    def test_db_sb_connection_strs(self):
        relation = mock.MagicMock()
        relation.relation_id = 'some-endpoint:42'
        self.patch_target('expand_name')
        self.expand_name.return_value = 'some-relation'
        self.patch_object(peers.ch_core.hookenv, 'network_get')
        self.network_get.return_value = {
            'bind-addresses': [
                {
                    'macaddress': '',
                    'interfacename': '',
                    'addresses': [
                        {
                            'hostname': '',
                            'address': '42.42.42.42',
                            'cidr': ''
                        },
                    ],
                },
            ],
            'egress-subnets': ['42.42.42.42/32'],
            'ingress-addresses': ['42.42.42.42'],
        }
        unit = mock.MagicMock()
        unit.received = {'bound-address': '1.2.3.4'}
        unit2 = mock.MagicMock()
        unit2.received = {'bound-address': '2.3.4.5'}
        relation.units.__iter__.return_value = [unit, unit2]
        self.patch_target('_relations')
        self._relations.__iter__.return_value = [relation]
        self.assertEquals(
            list(self.target.db_sb_connection_strs),
            ['ssl:42.42.42.42:16642',
             'ssl:1.2.3.4:16642',
             'ssl:2.3.4.5:16642'])

    def test_joined(self):
        self.patch_object(peers.reactive, 'set_flag')
        self.patch_object(peers.reactive, 'is_flag_set')
        self.patch_target('publish_cluster_local_addr')
        self.patch_target('expected_peers_available')
        self.expected_peers_available.return_value = False
        self.is_flag_set.return_value = False
        self.target.joined()
        self.expected_peers_available.assert_called_once_with()
        self.set_flag.assert_called_once_with('some-relation.connected')
        self.assertFalse(self.publish_cluster_local_addr.called)
        self.set_flag.reset_mock()
        self.expected_peers_available.return_value = True
        self.is_flag_set.return_value = True
        self.target.joined()
        self.publish_cluster_local_addr.assert_called_once_with()
        self.set_flag.assert_has_calls([
            mock.call('some-relation.connected'),
            mock.call('some-relation.available'),
        ])
