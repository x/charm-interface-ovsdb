# Copyright 2019 Canonical Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import mock

from ovsdb_cms import requires

import charms_openstack.test_utils as test_utils


_hook_args = {}


class TestOVSDBCMSProvides(test_utils.PatchHelper):

    def setUp(self):
        super().setUp()
        self.target = requires.OVSDBCMSRequires('some-relation', [])
        self._patches = {}
        self._patches_start = {}

    def tearDown(self):
        self.target = None
        for k, v in self._patches.items():
            v.stop()
            setattr(self, k, None)
        self._patches = None
        self._patches_start = None

    def patch_target(self, attr, return_value=None):
        mocked = mock.patch.object(self.target, attr)
        self._patches[attr] = mocked
        started = mocked.start()
        started.return_value = return_value
        self._patches_start[attr] = started
        setattr(self, attr, started)

    def patch_topublish(self):
        self.patch_target('_relations')
        relation = mock.MagicMock()
        to_publish = mock.PropertyMock()
        type(relation).to_publish = to_publish
        self._relations.__iter__.return_value = [relation]
        return relation.to_publish

    def test_publish_cms_client_bound_addr(self):
        self.patch_target('_endpoint_local_bound_addr')
        self._endpoint_local_bound_addr.return_value = 'a.b.c.d'
        to_publish = self.patch_topublish()
        self.target.publish_cms_client_bound_addr()
        to_publish.__setitem__.assert_called_once_with(
            'cms-client-bound-address', 'a.b.c.d')

    def test_joined(self):
        self.patch_target('publish_cms_client_bound_addr')
        self.patch_target('expected_units_available')
        self.patch_object(requires.reactive, 'set_flag')
        self.expected_units_available.return_value = False
        self.target.joined()
        self.publish_cms_client_bound_addr.assert_called_once_with()
        self.set_flag.assert_called_once_with('some-relation.connected')
        self.set_flag.reset_mock()
        self.expected_units_available.return_value = True
        self.target.joined()
        self.set_flag.assert_has_calls([
            mock.call('some-relation.connected'),
            mock.call('some-relation.available'),
        ])
