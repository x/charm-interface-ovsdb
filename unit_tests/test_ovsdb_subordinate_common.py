# Copyright 2020 Canonical Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import hashlib

from ovsdb_subordinate import ovsdb_subordinate_common as common

import charms_openstack.test_utils as test_utils


class TestOVSDBSubordinateCommon(test_utils.PatchHelper):

    def setUp(self):
        super().setUp()
        self._patches = {}
        self._patches_start = {}

    def tearDown(self):
        for k, v in self._patches.items():
            v.stop()
            setattr(self, k, None)
        self._patches = None
        self._patches_start = None

    def test_hash_hexdigest(self):
        s = 's'
        self.assertEquals(
            common.hash_hexdigest(s),
            hashlib.sha224(s.encode('utf8')).hexdigest())
        with self.assertRaises(TypeError):
            common.hash_hexdigest({})
