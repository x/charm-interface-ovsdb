# Copyright 2020 Canonical Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import charms.reactive as reactive

# the reactive framework unfortunately does not grok `import as` in conjunction
# with decorators on class instance methods, so we have to revert to `from ...`
# imports
from charms.reactive import (
    Endpoint,
    when,
    when_not,
)

from .ovsdb_subordinate_common import hash_hexdigest


class OVSDBSubordinateRequires(Endpoint):
    """This interface is used on a principle charm to connect to subordinate
    """

    @property
    def chassis_name(self):
        """Retrieve chassis-name from relation data

        :returns: Chassis name as provided on subordinate relation
        :rtype: str
        """
        return self.all_joined_units.received.get('chassis-name', '')

    @property
    def ovn_configured(self):
        """Retrieve whether OVN is configured from relation data

        :returns: True or False
        :rtype: bool
        """
        return self.all_joined_units.received.get('ovn-configured', False)

    @property
    def chassis_certificates(self):
        """Retrieve chassis certificates from relation data

        :returns: Certificate data
            {'ca_cert': '-----BEGIN ...',
             'certificate': '-----BEGIN ...',
             'private_key': '-----BEGIN ...'}
        :rtype: Dict[str,str]
        """
        return self.all_joined_units.received.get('chassis-certificates', {})

    def _add_interface_request(self, bridge, ifname, ifdata):
        """Retrieve interface requests from relation and add/update requests

        :param bridge: Name of bridge
        :type bridge: str
        :param ifname: Name of interface
        :type ifname: str
        :param ifdata: Data to be attached to interface in Open vSwitch
        :type ifdata: Dict[str,Union[str,Dict[str,str]]]
        """
        for relation in self.relations:
            relation_ifs = relation.to_publish.get('create-interfaces', {})
            relation_ifs.update({bridge: {ifname: ifdata}})
            relation.to_publish['create-interfaces'] = relation_ifs

    def _interface_requests(self):
        """Retrieve interface requests from relation

        :returns: Current interface requests
        :rtype: Optional[Dict[str,Union[str,Dict[str,str]]]]
        """
        for relation in self.relations:
            return relation.to_publish_raw.get('create-interfaces')

    def create_interface(self, bridge, ifname, ethaddr, ifid,
                         iftype=None, ifstatus=None):
        """Request system interface created and attach it to CMS

        Calls to this function are additive so a principle charm can request to
        have multiple interfaces created and maintained.

        The flag {endpoint_name}.{interface_name}.created will be set when
        ready.

        :param bridge: Bridge the new interface should be created on
        :type bridge: str
        :param ifname: Interface name we want the new netdev to get
        :type ifname: str
        :param ethaddr: Ethernet address we want to attach to the netdev
        :type ethaddr: str
        :param ifid: Unique identifier for port from CMS
        :type ifid: str
        :param iftype: Interface type, defaults to 'internal'
        :type iftype: Optional[str]
        :param ifstatus: Interface status, defaults to 'active'
        :type ifstatus: Optional[str]
        """
        # The keys in the ifdata dictionary map directly to column names in the
        # OpenvSwitch Interface table as defined in DB-SCHEMA [0] referenced in
        # RFC 7047 [1]
        #
        # There are some established conventions for keys in the external-ids
        # column of various tables, consult the OVS Integration Guide [2] for
        # more details.
        #
        # NOTE(fnordahl): Technically the ``external-ids`` column is called
        # ``external_ids`` (with an underscore) and we rely on ``ovs-vsctl``'s
        # behaviour of transforming dashes to underscores for us [3] so we can
        # have a more pleasant data structure.
        #
        # 0: http://www.openvswitch.org/ovs-vswitchd.conf.db.5.pdf
        # 1: https://tools.ietf.org/html/rfc7047
        # 2: http://docs.openvswitch.org/en/latest/topics/integration/
        # 3: https://github.com/openvswitch/ovs/blob/
        #        20dac08fdcce4b7fda1d07add3b346aa9751cfbc/
        #            lib/db-ctl-base.c#L189-L215
        ifdata = {
            'type': iftype or 'internal',
            'external-ids': {
                'iface-id': ifid,
                'iface-status': ifstatus or 'active',
                'attached-mac': ethaddr,
            },
        }
        self._add_interface_request(bridge, ifname, ifdata)
        reactive.clear_flag(
            self.expand_name('{endpoint_name}.interfaces.created'))

    @when('endpoint.{endpoint_name}.joined')
    def joined(self):
        reactive.set_flag(self.expand_name('{endpoint_name}.connected'))
        if self.chassis_name:
            reactive.set_flag(self.expand_name('{endpoint_name}.available'))
        else:
            reactive.clear_flag(self.expand_name('{endpoint_name}.available'))

    @when_not('endpoint.{endpoint_name}.joined')
    def broken(self):
        reactive.clear_flag(self.expand_name('{endpoint_name}.available'))
        reactive.clear_flag(self.expand_name('{endpoint_name}.connected'))

    @when('endpoint.{endpoint_name}.changed.interfaces-created')
    def new_requests(self):
        ifreq = self._interface_requests()

        if ifreq is not None and self.all_joined_units.received[
                'interfaces-created'] == hash_hexdigest(ifreq):
            reactive.set_flag(
                self.expand_name('{endpoint_name}.interfaces.created'))
            reactive.clear_flag(
                self.expand_name(
                    'endpoint.{endpoint_name}.changed.interfaces-created'))
