# Copyright 2018 Canonical Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# the reactive framework unfortunately does not grok `import as` in conjunction
# with decorators on class instance methods, so we have to revert to `from ...`
# imports

import inspect

import charmhelpers.core as ch_core

import charms.reactive as reactive

from charms.reactive import (
    when,
)

from .lib import ovsdb as ovsdb


class OVSDBCMSRequires(ovsdb.OVSDB):

    def publish_cms_client_bound_addr(self):
        """Announce address on relation.

        This will be used by our peers to grant access.
        """
        for relation in self.relations:
            relation.to_publish['cms-client-bound-address'] = (
                self._endpoint_local_bound_addr())

    @when('endpoint.{endpoint_name}.joined')
    def joined(self):
        ch_core.hookenv.log('{}: {} -> {}'
                            .format(self._endpoint_name,
                                    type(self).__name__,
                                    inspect.currentframe().f_code.co_name),
                            level=ch_core.hookenv.INFO)
        super().joined()
        self.publish_cms_client_bound_addr()
        if self.expected_units_available():
            reactive.set_flag(self.expand_name('{endpoint_name}.available'))

    @when('endpoint.{endpoint_name}.broken')
    def broken(self):
        super().broken()
