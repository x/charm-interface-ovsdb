# Copyright 2018 Canonical Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import itertools

import charmhelpers.core as ch_core

import charms.reactive as reactive

# the reactive framework unfortunately does not grok `import as` in conjunction
# with decorators on class instance methods, so we have to revert to `from ...`
# imports
from charms.reactive import (
    when,
)

from .lib import ovsdb as ovsdb


class OVSDBClusterPeer(ovsdb.OVSDB):
    DB_SB_ADMIN_PORT = 16642
    DB_NB_CLUSTER_PORT = 6643
    DB_SB_CLUSTER_PORT = 6644

    @property
    def db_sb_admin_port(self):
        """Provide admin port number for OVN Southbound OVSDB.

        This is a special listener to allow ``ovn-northd`` to connect to an
        endpoint without RBAC enabled as there is currently no RBAC profile
        allowing ``ovn-northd`` to perform its work.

        :returns: admin port number for OVN Southbound OVSDB.
        :rtype: int
        """
        return self.DB_SB_ADMIN_PORT

    @property
    def db_nb_cluster_port(self):
        """Provide port number for OVN Northbound OVSDB.

        :returns port number for OVN Northbound OVSDB.
        :rtype: int
        """
        return self.DB_NB_CLUSTER_PORT

    @property
    def db_sb_cluster_port(self):
        """Provide port number for OVN Southbound OVSDB.

        :returns: port number for OVN Southbound OVSDB.
        :rtype: int
        """
        return self.DB_SB_CLUSTER_PORT

    def expected_peers_available(self):
        """Whether expected peers have joined and published data on peer rel.

        NOTE: This does not work for the normal inter-charm relations, please
              refer separate method for that in the shared interface library.

        :returns: True if expected peers have joined and published data,
                  False otherwise.
        :rtype: bool
        """
        if len(self.all_joined_units) == len(
                list(ch_core.hookenv.expected_peer_units())):
            for relation in self.relations:
                for unit in relation.units:
                    if not unit.received.get('bound-address'):
                        return False
            return True
        return False

    @property
    def db_nb_connection_strs(self):
        """Provide Northbound DB connection strings.

        We override the parent property because for the peer relation
        ``cluster_remote_addrs`` does not contain self.

        :returns: Northbound DB connection strings
        :rtype: Iterator[str]
        """
        return itertools.chain(
            self.db_connection_strs((self.cluster_local_addr,),
                                    self.db_nb_port),
            self.db_connection_strs(self.cluster_remote_addrs,
                                    self.db_nb_port))

    @property
    def db_sb_connection_strs(self):
        """Provide Southbound DB connection strings.

        We override the parent property because for the peer relation
        ``cluster_remote_addrs`` does not contain self.  We use a different
        port for connecting to the SB DB as there is currently no RBAC profile
        that provide the privileges ``ovn-northd`` requires to operate.

        :returns: Southbound DB connection strings
        :rtype: Iterator[str]
        """
        return itertools.chain(
            self.db_connection_strs((self.cluster_local_addr,),
                                    self.db_sb_admin_port),
            self.db_connection_strs(self.cluster_remote_addrs,
                                    self.db_sb_admin_port))

    @when('endpoint.{endpoint_name}.joined')
    def joined(self):
        super().joined()
        if reactive.is_flag_set('leadership.set.ready'):
            self.publish_cluster_local_addr()
        if self.expected_peers_available():
            reactive.set_flag(self.expand_name('{endpoint_name}.available'))

    @when('endpoint.{endpoint_name}.broken')
    def broken(self):
        super().broken()
